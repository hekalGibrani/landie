<!DOCTYPE html>
<html lang="en">
	<!--[if lt IE 7 ]><html dir="ltr" lang="en-US" class="no-js ie ie6 lte7 lte8 lte9"><![endif]-->
	<!--[if IE 7 ]><html dir="ltr" lang="en-US" class="no-js ie ie7 lte7 lte8 lte9"><![endif]-->
	<!--[if IE 8 ]><html dir="ltr" lang="en-US" class="no-js ie ie8 lte8 lte9"><![endif]-->
	<!--[if IE 9 ]><html dir="ltr" lang="en-US" class="no-js ie ie9 lte9"><![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!-->
	<html dir="ltr" lang="en-US">
	<!--<![endif]-->
	<head>

		<title>Landie</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="shortcut icon" href="img/favicon.png" />
		
		<link href="css/style.css" type="text/css" rel="stylesheet" />


	</head>

	<body>


        <div id="header" class="grid w100 header">
            <div class="nav">
                <a href="javascript:void(0);">Home</a>
                <a href="javascript:void(0);">About</a>
                <a href="javascript:void(0);">Contact</a>
            </div>
            <div class="logo"><a href="javascript:void(0);"><img src="img/logo.png" /></a></div>
            <button>Live Now</button>
        </div>